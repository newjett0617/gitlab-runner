#!/usr/bin/env bash

PROJECT_REGISTRATION_TOKEN=<your-token>
docker exec gitlab-runner gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $PROJECT_REGISTRATION_TOKEN \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --tag-list "" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
