#!/usr/bin/env bash

docker run -d \
  --name gitlab-runner \
  --restart always \
  --network bridge \
  -v gitlab-runner-home:/home/gitlab-runner:rw \
  -v gitlab-runner-config:/etc/gitlab-runner:rw \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  docker.io/gitlab/gitlab-runner:latest
